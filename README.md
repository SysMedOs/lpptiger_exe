#### * This repository is a for the legacy LPPtiger version originally publishesd in 2017.
#### * This project is now moved to LPPtiger 2 repository at [https://github.com/LMAI-TUD/lpptiger2](https://github.com/LMAI-TUD/lpptiger2)

# LPPtiger 2

![LPPtiger2_new_features](https://pbs.twimg.com/media/FV7BsPzXkAAKq-x?format=jpg)

## About LPPtiger 2

LPPtiger 2 provides you a set of powerful functions to explor epilipidome.

Main functions:

+ Prediction of epilipidome

+ in silico fragmentation of epilipids

+ Generation of inclusion list

+ Identify epilipids at discrete level with modification type information

[Check the latest release of LPPtiger2 ⬇](https://github.com/LMAI-TUD/lpptiger2/releases)

LPPtiger 2 provides user friendly intuitive graphic interface for multiple platforms including macOS (Apple silicon), Linux, and Windows (Win10, Win 11).


# For the original LPPtiger

# LPPtiger Instructions (Windows executable version)#

![LPPtiger_exe_Banner.png](https://bitbucket.org/repo/kMMgepG/images/2146023823-LPPtiger_exe_Banner.png)

** This repository contains the the Windows .exe executable version LPPtiger. **

LPPtiger Windows executable files were generated from source code using [`PyInstaller`](http://www.pyinstaller.org). 

LPPtiger source code repository can be found here
https://bitbucket.org/SysMedOs/lpptiger

Here we will provide general information how to download Windows executable version.

** System requirements **
 
LPPtiger Windows executable version is provided for ** 64bit ** version of **Windows 7, 8, 8.1 and 10 ** only.

LPPtiger requires minimum 2.0 GHz CPU with 2 cores and 4 GB RAM to run (8 GB or 16 GB of RAM are recommended for multiple processing)



**Please download and read LPPtiger user guide before you start to run LPPtiger. **

+ [Download LPPtiger user guide PDF](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtiger_User_Guide.pdf)
+ [https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtiger_User_Guide.pdf](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtiger_User_Guide.pdf)

**We also provide a set of sample spectra and output files as a tutorial packages for you to get start with LPPtiger. **

+ [Download LPPtiger tutorial file package as .zip](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtigerTutorial.zip)
+ [https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtigerTutorial.zip](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtigerTutorial.zip)


** Note: **

Though we try our best to keep Windows executable distribution up to date, we have no guarantee that it corresponds to the latest version of LPPtiger source code. Please check out the original LPPtiger repository for latest updates.

If you have any problems while using LPPtiger, please report it here: [https://bitbucket.org/SysMedOs/lpptiger/issues](https://bitbucket.org/SysMedOs/lpptiger/issues)



** Please read the following instructions before you start to run LPPtiger. **

### Instructions ###

* [How to install Windows executable version of LPPtiger](#markdown-header-how-to-install-windows-executable-version-of-lpptiger)
* [License](#markdown-header-license)
* [A step by step tutorial](https://bitbucket.org/SysMedOs/lpptiger/wiki/Home)
* [Q&A](#markdown-header-further-questions)
* [Fundings](#markdown-header-fundings)


### How to install Windows executable version of LPPtiger ###

* **Download and read LPPtiger user guide:** [Download LPPtiger user guide PDF](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtiger_User_Guide.pdf)
    
    + [https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtiger_User_Guide.pdf](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtiger_User_Guide.pdf)

* Download the LPPtiger suitable for your system:

    + LPPtiger and related files can be downloaded from: https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/

        - [LPPtiger for Windows 10 64bit ( `.zip` file around 230 MB)](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtiger_Win10_64bit.zip)
        - [LPPtiger for Windows 7,8 and 8.1 64bit ( `.zip` file around 240 MB)](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtiger_Win7_64bit.zip)
    
* Rename the downloaded file  to `LPPtiger.zip`
    
* Unzip `LPPtiger.zip` file to any folder. We recommend to use `7-zip` to unzip this file.

    + 7-Zip is open source software. [7-Zip home page](http://www.7-zip.org)
            
* **Optional: Resolve `.dll` issues. (Only do this step if LPPtiger do NOT run properly on your system)**

    LPPtiger is a free open source software. However, some system files might be required to execute the LPPtiger.exe. If you receive .dll error from LPPtiger or LPPtiger can not start, try to copy following system files to the LPPtiger folder and try again.

    + Copy following `.dll` and `.DRV` files from your system folder: `C:\WINDOWS\system32\` to the LPPtiger folder:

        + `.DRV` files:
        
                WINSPOOL.DRV - C:\WINDOWS\system32\WINSPOOL.DRV
            
        + `.dll` files:
    
                OLEAUT32.dll - C:\WINDOWS\system32\OLEAUT32.dll
                USER32.dll - C:\WINDOWS\system32\USER32.dll
                IMM32.dll - C:\WINDOWS\system32\IMM32.dll
                SHELL32.dll - C:\WINDOWS\system32\SHELL32.dll
                ole32.dll - C:\WINDOWS\system32\ole32.dll
                ODBC32.dll - C:\WINDOWS\system32\ODBC32.dll
                COMCTL32.dll - C:\WINDOWS\system32\COMCTL32.dll
                ADVAPI32.dll - C:\WINDOWS\system32\ADVAPI32.dll
                SHLWAPI.dll - C:\WINDOWS\system32\SHLWAPI.dll
                WS2_32.dll - C:\WINDOWS\system32\WS2_32.dll
                GDI32.dll - C:\WINDOWS\system32\GDI32.dll
                WINMM.dll - C:\WINDOWS\system32\WINMM.dll
                VERSION.dll - C:\WINDOWS\system32\VERSION.dll
                KERNEL32.dll - C:\WINDOWS\system32\KERNEL32.dll
                COMDLG32.dll - C:\WINDOWS\system32\COMDLG32.dll
                OPENGL32.dll - C:\WINDOWS\system32\OPENGL32.dll
            
        + For windows 7, 8, 8.1 additional `.dll` files might be required:
        
                msvcrt.dll - C:\WINDOWS\system32\msvcrt.dll
                ntdll.dll - C:\WINDOWS\system32\ntdll.dll

* Start LPPtiger by `LPPtiger.exe` 
    + The first time to start LPPtiger may take relative long time, please allow the software to run if LPPtiger got blocked by your anti-virus software.
    + We recommend you to create a shortcut of `LPPtiger.exe` on your desktop. The Windows 10 version shortcut comes with built in LPPtiger icon, and users of Windows 7-8 versions can find the `LPPtiger.ico` in LPPtiger folder to customize the shortcut.  
  
* Run the test files
    
    [LPPtiger Tutorial package with sample mzML files](https://bitbucket.org/SysMedOs/lpptiger_exe/downloads/LPPtigerTutorial.zip)

* Run your data

* Errors/bugs
    
    In case you experienced any problems with running LPPtiger, please report an issue in the [issue tracker](https://bitbucket.org/SysMedOs/lpptiger/issues) or contact us.

### License ###

+ LPPtiger is Dual-licensed
    * For academic and non-commercial use: `GPLv2 License`: 
    
        [The GNU General Public License version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

    * For commercial use: please contact the develop team by email.

+ Please cite our publication in an appropriate form. 

     + [Ni, Zhixu, Georgia Angelidou, Ralf Hoffmann, and Maria Fedorova. "LPPtiger software for lipidome-specific prediction and identification of oxidized phospholipids from LC-MS datasets." Scientific Reports 7, Article number: 15138 (2017).](https://www.nature.com/articles/s41598-017-15363-z)
    
        * DOI: [`10.1038/s41598-017-15363-z`](https://www.nature.com/articles/s41598-017-15363-z)


### Further questions? ###

* Read our [wiki](https://bitbucket.org/SysMedOs/lpptiger/wiki/Home)
* Report any issues here: [https://bitbucket.org/SysMedOs/lpptiger/issues](https://bitbucket.org/SysMedOs/lpptiger/issues)


### Fundings ###
We acknowledge all projects that supports the development of LPPtiger:

+ BMBF - Federal Ministry of Education and Research Germany:

    https://www.bmbf.de/en/

+ e:Med Systems Medicine Network:

    http://www.sys-med.de/en/

+ SysMedOS Project : 

    https://home.uni-leipzig.de/fedorova/sysmedos/